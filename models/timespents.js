const mongoose = require("mongoose");
// const SurveyTypes = require("../graphql/types/surveys");
const SurveySchema = new mongoose.Schema(
  {
    userEmail: { type: String },
    pageid: { type: String  },
    time: { type: String}
  },
  {
    timestamps: true
  }
);

const timeModel = mongoose.model("timespents", SurveySchema);
module.exports = timeModel;
