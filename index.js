const http = require("http");
const express = require("express");
const inforUser = require("./models/users");
const gameModel = require("./models/game");
const mongoose = require("mongoose");
const nodemailer = require("nodemailer");
const timeModel = require("./models/timespents");
const timeModelLogins = require("./models/loginstasts");
const zoneVisit = require("./models/zonevisit");

const Oneusers = require("./models/userones")
const jwt = require("jsonwebtoken");
var cors = require("cors");

mongoose.set("useFindAndModify", false);

const bodyParser = require("body-parser");


const app = express();


app.use(
  bodyParser.urlencoded({
    extended: false,
  })
);


// parse application/json
app.use(bodyParser.json());

 //app.use(cors());
var port = process.env.PORT || 3001;

var jsonParser = bodyParser.json();



var urlencodedParser = bodyParser.urlencoded({
  extended: false,
});
const server = http.createServer(app);
//const uri ="mongodb+srv://testing:9kH62kPUDio9OleA@cluster0.kw5yl.mongodb.net/test3?retryWrites=true&w=majority";
 const uri ="mongodb://localhost:27017/test"
//const uri ="mongodb://13.213.200.139:27017/?serverSelectionTimeoutMS=5000&connectTimeoutMS=10000&3t.uriVersion=3&3t.connection.name=IBC&3t.ssh=true&3t.sshAddress=13.213.200.139&3t.sshPort=22&3t.sshAuthMode=privateKey&3t.sshUser=ec2-user&3t.sshPKPath=C:\Users\Srinath\Downloads\lbc_expokeypair.pem&3t.sshUsePKPassphrase=false&3t.alwaysShowAuthDB=true&3t.alwaysShowDBFromUserRole=true";
mongoose
  .connect(uri, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })

  .then(() => {
    server.listen(port, () =>
      console.log(`Example app listening on port port!m  and database ${port}`)
    );
  })
  .catch((err) => console.log(err));

app.get("/", (req, res) => {
  res.send("Hello World, from express ji222lbc");
});

app.get("/alluser", async (req, res) => {
  let docs = await inforUser.find({});
console.log(docs)
  

  res.send({
count:docs.length,      data:docs});
});
app.post("/findoneuser", async (req, res) => {
  console.log("sss", req.body);
  let emailtest = await inforUser.findOne({
    userEmail: req.body.userEmail,
  });


   

    if (emailtest) {
      res.send({
        data: emailtest,
      });
    } else {
      res.status(201).send({
        data: "notfound",
      });
    }
  
});

app.post("/gamefind", async (req, res) => {
  let docs = await gameModel.find({
    gameName: req.body.gameName,
    email: req.body.email,
  });
  let docsALL = await gameModel
    .find({
      gameName: req.body.gameName,
    })
    .sort({
      userScore: -1,
    })

    .limit(2000);
  var finalData = [];
  console.log("finalData", finalData, docsALL);
  docsALL.map((item, index) => {
    finalData.push({
      gameName: item.gameName,

      userScore: item.userScore,
      name: item.name,
      email: item.email,
      rank: index + 1,
    });
  });

  // console.log("finalData",finalData,docsALL)
  res.send({
    data: {
      docs: docs,
      finalData,
    },
  });
});

app.post("/gamedataenter", async (req, res) => {
  console.log("sss", JSON.stringify(req.body));

  // gameModel.findOneAndUpdate({gameName:req.body.gameName ,phoneNumber:req.body.phoneNumber },null, function (err, docs) {

  let check = await gameModel.find({
    gameName: req.body.gameName,
    email: req.body.email,
  });
  console.log("dataaa", check);
  if (check.length > 0) {
    

    try {
      if (parseInt(check[0].userScore) < parseInt(req.body.userScore)) {
        gameModel.findOneAndUpdate(
          {
            gameName: req.body.gameName,
            email: req.body.email,
          },
          {
            userScore: req.body.userScore,
          },
          function (err, result) {
            if (err) {
              res.send(err);
            } else {
              res.send(result);
            }
          }
        );

        //   gameModel.updateOne({userScore:parseInt(req.body.userScore)}, function (err, docs) {
        //     if (err){
        //         console.log(err)
        //     }
        //     else{
        //         console.log("Updated Docs : ", docs);
        //         console.log(docs)
        //         return  res.send(docs);
        //     }
        // })
      } else {
        return res.send({
          data: "alredy high score",
        });
      }
    } catch (error) {
      return error;
    }
  } else {
    let newgame = new gameModel({
      gameName: req.body.gameName,

      userScore: parseInt(req.body.userScore),
      name: req.body.name,
      email: req.body.email,
    });
    try {
      let response = await newgame.save();
      res.send(response);
      return response;
    } catch (error) {
      return error;
    }
  }
});

app.post("/mailsend", async (req, res) => {
 



  if (req.body.confirmpassword !== req.body.password) {
    res.send({
      data: "passwordnotmtach",
    });
  }

  let emailtest = await inforUser.find({
    email: req.body.email,
  });

  if (emailtest.length > 0 ) {
    res.send({
      data: "alredyRegister",
    });
  }

  if (emailtest.length > 0) {
    inforUser.findOneAndUpdate(
      {
        email: req.body.email,
      },
      {
        password: req.body.password,
        register: "register",
      },
      function (err, result) {
        if (err) {
          res.send(err);
        } else {
          res.send({
            data: "changed",
          });
        }
      }
    );
  } else {
    res.send({
      data: "not",
    });
  }

  if (emailtest.length > 0) {
    // async..await is not allowed in global scope, must use a wrapper
    async function main() {
      // Generate test SMTP service account from ethereal.email
      // Only needed if you don't have a real mail account for testing
      let testAccount = await nodemailer.createTestAccount();

      // create reusable transporter object using the default SMTP transport
      let transporter = nodemailer.createTransport({
        service: "gmail",
        port: 587,
        secure: false, // true for 465, false for other ports
        auth: {
          user: "noreply@quest-emm.virtuallive.in",
          pass: "sppl2014",
        },
      });

      // send mail with defined transport object
      let info = await transporter.sendMail({
        from: req.body.email, // sender address
        to: `${req.body.email}`, // list of receivers
        subject: "Hello ✔", // Subject line
        text: "Hello world?", // plain text body
        html: `<br>Hi ${req.body.email} <br> your password is ${req.body.password}  `, // html body
      });

      console.log("Message sent: %s", info.messageId);
      // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>

      // Preview only available when sending through an Ethereal account
      console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));

      // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
    }

    main().catch(console.error);
  }
});

app.post("/registration", async (req, res) => {
 

//   if (req.body.confirmpassword !== req.body.password) {
//     res.send({
//       data: "passwordnotmtach",
//     });
   
//   }

  let emailtest = await inforUser.findOne({
    userEmail: req.body.userEmail,
  });

  if (emailtest ) {
    res.status(200).send({
      data:'already',
    });
  }
else

{
    console.log("9999")
  var newUser = new inforUser({
   
    userEmail: req.body.userEmail,

    userName: req.body.userName
  })
  let response = await newUser.save();
  const SENDGRID_API_KEY =
  "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";
const sgMail = require("@sendgrid/mail");
sgMail.setApiKey(SENDGRID_API_KEY);
// console.log(req.body)
// const content=req.body.imgurl
const msg = {
  to: req.body.userEmail, // Change to your recipient
  from: "noreply@svbgdcannualday2022.virtuallive.in", // Change to your verified sender
  subject: " Tarang 2022 Registration ",
  html: `Hello,
<br/>
<br/>
Thank you for registering on the Tarang 2022,
<br/>
<br/>
Event Date : January 7, 2022
<br/>

URL: https://svbgdcannualday2022.virtuallive.in/
<br/>
<br/>
Save this email to refer your log-in credentials if you have any trouble logging in.
<br/>
<br/>
<b>  Log-in credentials: </b>
<br/>
<br/>
<b>Name:</b>  ${req.body.userName}
<br/>


<b>Email Id:</b> ${req.body.userEmail}
<br/>

<br/>
This is an autogenerated email. Do not reply to this email. Do not delete / archive this mail till January 07, 2022
<br/>
<br/>
Please do not forget to block your time in your calendar.
<br><br>

 Warm Regards
 <br>
 Tarang 2022
<br>
Support Team
 <br/> <br/>
 
  `,
};
sgMail
  .send(msg)
  .then(async () => { 
    
    console.log("ghjkl",response)
    res.send({
      data: "changed"
    });
   })
  .catch((error) => {
    console.error(error);
    res.send({ data: "Notdone" });
  })


   
   
}


//   if (true) {
//     // async..await is not allowed in global scope, must use a wrapper
//     async function main() {
//       // Generate test SMTP service account from ethereal.email
//       // Only needed if you don't have a real mail account for testing
//       let testAccount = await nodemailer.createTestAccount();

//       // create reusable transporter object using the default SMTP transport
//       let transporter = nodemailer.createTransport({
//         service: "gmail",
//         port: 587,
//         secure: false, // true for 465, false for other ports
//         auth: {
//           user: "virtualliveevent@gmail.com",
//           pass: "Sppl2014",
//         },
//       });

//       // send mail with defined transport object
//       let info = await transporter.sendMail({
//         from: req.body.email, // sender address
//         to: `${req.body.email}`, // list of receivers
//         subject: "Hello ✔", // Subject line
//         text: "Hello world?", // plain text body
//         html: `<br>Hi ${req.body.email} <br> your password is ${req.body.password}  `, // html body
//       });

//       console.log("Message sent: %s", info.messageId);
//       // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>

//       // Preview only available when sending through an Ethereal account
//       console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));

//       // Preview URL: https://ethereal.email/message/WaQKMgKddxQDoou...
//     }

//     main().catch(console.error);
//   }
});



app.post('/timespent', async(req, res) => {

 

    var newtimedata = new timeModel({
       
        pageid: req.body.pageid,
        time: req.body.time,
        userEmail: req.body.userEmail,
       
      });
      console.log("newtimedata",newtimedata);
      try {
        let response = await newtimedata.save();
        console.log("response",response);
        res.send(({data:response}))
    
        return response;
      } catch (error) {
        return error;
      }
   }
   )
  


app.get("/createuser", async(req, res) => {
console.log(
  "009999"
)
dataa.map(async(item)=>{

var newUser = new inforUser({
name: item.name,
age: item.age,
phoneNumber: item.phoneNumber,
status: item.status,
email: item.email,
token: item.token,
password: item.password
})
try {
let response = await newUser.save();
// res.send(response);
console.log(response);
} catch (error) {
return error;
}


}
)
}
)




app.post("/lastlogin", async (req, res) => {
  console.log(req.body);
  let emailtest = await timeModelLogins.find({
    userEmail: req.body.userEmail,
  });
  console.log("otpTestotpTest", emailtest);
  if (emailtest && emailtest.length > 0) {
    // console.log(new Date( emailtest[0].updatedAt))

    // let date = new Date(emailtest[0].updatedAt);

    // convert Date object UNIX timestamp

    if (emailtest.length > 0) {
      timeModelLogins.findOneAndUpdate(
        {
          userEmail: req.body.userEmail,
        },
        {
          time: new Date(emailtest[0].updatedAt),
        },
        function (err, result) {
          if (err) {
            res.send(err);
          } else {
            // console.log('111www')
            res.send(result);
          }
        }
      );
    }
  } else {
    let newtime = new timeModelLogins({
      userEmail: req.body.userEmail,
      userName: req.body.userName,

      time: 3,
    });
    let response2 = await newtime.save();
    console.log("www");
    res.send(response2);
  }
});




app.get("/totalregister", async (req, res) => {
  console.log("sss", req.body);
  let emailtest = await inforUser.find({
  
  })

 let jsonObject = emailtest.map(JSON.stringify);
      
            console.log(jsonObject);
      
            uniqueSet = new Set(jsonObject);
            uniqueArray = Array.from(uniqueSet).map(JSON.parse);
      

 
let count= uniqueArray.length
let emailtestCount=emailtest.length
  res.send({emailtestCount,count,emailtest})
}

)



function generateAccessToken(username) {
  return jwt.sign(username, require("crypto").randomBytes(64).toString("hex"));
}

app.post("/loginonetime", async (req, res) => {

  let emailIdExist = await Oneusers.findOne({
    userEmail: req.body.userEmail,
   
  })
  if(!emailIdExist)
  {
   return res.status(201).send("user Not Found");
  }


  let emailIdExist2 = await Oneusers.findOne({
    userEmail: req.body.userEmail,
    passWord: req.body.passWord,

   
  })
if(emailIdExist&& !emailIdExist2)
{
 return res.status(200).send({data:"password is wrong"});
}

//   else{




 
//     res.status(200).send(userExist);

  
 

// }

  if(true)
  {
  if  ( emailIdExist) {
    console.log("update");

    Oneusers.findOneAndUpdate(
      {
        userEmail: req.body.userEmail,
      },
      {
        userToken: generateAccessToken(req.body.userEmail),
      },
      { new: true },
      function (err, result) {
        if (err) {
          res.send(err);
        } else {
         return res.send(result);
        }
      }
    );
  } else {
    console.log("update nahi");

   
  
  }
}
});

app.post("/registeronetime", async (req, res) => {
  let userExist = await Oneusers.findOne({
    userEmail: req.body.userEmail,
    userName: req.body.userName,
  });

  if (userExist) {
    console.log("update");

    res.send("already resgister");
  } else {
    console.log("update nahi");

    let storeData = await new Oneusers({
      userEmail: req.body.userEmail,
      userName: req.body.userName,
      userToken: generateAccessToken(req.body.userEmail),
    });

    let sendData = await storeData.save();

    res.send(sendData);
  }
});

app.post("/getoken", async (req, res) => {
  let userExistToken = await Oneusers.findOne({
    userEmail: req.body.userEmail,
    userToken: req.body.userToken,
  });

  if (userExistToken) {
    console.log("update");

    res.send("true");
  } else {
    res.send("false");
  }
});

app.post("/enterUser", async (req, res) => {
let storeData = await new Oneusers({
  userEmail: req.body.userEmail,
  passWord: req.body.passWord,
  userToken: generateAccessToken(req.body.userEmail),
});
let sendData = await storeData.save();
res.send(sendData);
}
)

app.post("/zonevisit", async (req, res) => {
  let userExistToken2 = await zoneVisit.findOne({
    userEmail: req.body.userEmail,
    userName:  req.body.userName,
    zoneName: req.body.zoneName,
  })

  if(userExistToken2)
  {

res.send("datata")
  
}
else{


let storeData = await new zoneVisit({
  userEmail: req.body.userEmail,
  userName:  req.body.userName,
  zoneName: req.body.zoneName,
});
  let sendData = await storeData.save();
  res.send(sendData);
  }
}
  )



  
app.post("/zoneallperson", async (req, res) => {
  let userExistToken2 = await zoneVisit.find({
   
    zoneName: req.body.zoneName,
  })

 res.send({count:userExistToken2.length,data:userExistToken2})

}

  )
  app.post("/zonetimeperson", async (req, res) => {
    let userExistToken2 = await zoneVisit.find({
     
      userEmail: req.body.userEmail,
    })
  
   res.send({count:userExistToken2.length,data:userExistToken2})
  
  }
  
    )